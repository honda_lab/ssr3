# ssr3

スキッドステアリングロボット ver.3 (SSR3)のボディーデータ(STL)を
ここに置きます．

<img src="https://gitlab.com/honda_lab/chainer_ssr3/-/raw/main/SSR3a.JPG" width=480>

[走行動画](https://muroranit-my.sharepoint.com/personal/19026220_mmm_muroran-it_ac_jp/_layouts/15/onedrive.aspx?id=%2Fpersonal%2F19026220%5Fmmm%5Fmuroran%2Dit%5Fac%5Fjp%2FDocuments%2F%E5%AE%9F%E9%A8%93%E8%A8%98%E9%8C%B22022%2F3%E5%B9%B4%E7%94%9F%2F20220610%5F085537000%5FiOS%2EMOV&parent=%2Fpersonal%2F19026220%5Fmmm%5Fmuroran%2Dit%5Fac%5Fjp%2FDocuments%2F%E5%AE%9F%E9%A8%93%E8%A8%98%E9%8C%B22022%2F3%E5%B9%B4%E7%94%9F&ga=1)
